<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/form' , 'FormController@form');

Route::post('/storeData' , 'FormController@store')->name('storeData');


Route::get('/dropdown' , 'DropdownController@index')->name('dropdown');

Route::get('/getCities/{id}' , 'DropdownController@getCities')->name('getCities');

Route::any('/getData' , 'DropdownController@getData')->name('getData');


