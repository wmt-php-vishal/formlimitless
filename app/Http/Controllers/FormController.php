<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class FormController extends Controller
{
    public function form()
    {
        return view('form');
    }

    public function store(Request $request)
    {

//dd($request->all());
//        dd($request->usernamePrepend.$request->username);
//        dd($request->hobbies);
//        $hobbies = $request->input('hobbies');
//        $hobbies = implode(',' , $hobbies);

//        dd($request->hobbies[0].",".$request->hobbies[1].",".$request->hobbies[2].",".$request->hobbies[3].",".$request->hobbies[4]);
        $student = new Student();

        $student->name = $request->name;
        $student->address = $request->address;
        $student->password = $request->password;
        $student->url = $request->url;

        $student->hobbies = $request->hobbies[0].",".$request->hobbies[1].",".$request->hobbies[2].",".$request->hobbies[3].",".$request->hobbies[4];
        $student->gender = $request->gender;
        $student->subject = $request->sub1." ".$request->sub2." ".$request->sub3;
        $student->username = $request->usernamePrepend.$request->username;
        $student->email = $request->email.$request->emailAppend;

        $student->save();

        if ($student->save())
        {
            echo "data saved successfully";
//            dd($student);
        }
        else
        {
            dd('failed');
        }
    }




}
