@extends('html')


@section('js')

    {{--form basic input js--}}
    <script type="text/javascript" src="{{asset('assets/js/core/app.js')}}"></script>

    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/form_inputs.js')}}"></script>
    {{--form basic input js--}}

    {{--form checkbox radio js--}}

    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/plugins/forms/styling/switch.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/pages/form_checkboxes_radios.js')}}"></script>
    {{--form checkbox radio js--}}

    {{--file upload js--}}
    <script type="text/javascript" src="{{ asset('assets/js/plugins/uploaders/fileinput.min.js')}}"></script>
    <script type="text/javascript" src="{{ url('assets/js/pages/uploader_bootstrap.js') }}"></script>
    {{--file upload js end here--}}




    <!-- /theme JS files -->

@endsection

@section('title', 'Form')

@section('content')

    <div class="panel panel-flat container">

        <div class="panel-body ">
        <h2>Registration Form</h2>

            <form class="form-horizontal" action="{{ route('storeData') }}" method="Post" enctype="multipart/form-data">
                @csrf
                <fieldset class="content-group">
                    <legend class="text-bold">Basic inputs</legend>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Name</label>
                        <div class="col-lg-10">
                            <input type="text" class="form-control" name="name" placeholder="Enter Your Name" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Password</label>
                        <div class="col-lg-10">
                            <input type="password" class="form-control" name="password" placeholder="Password" autocomplete="off">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-lg-2 control-label">File upload:</label>
                        <div class="col-lg-10">
                            <input type="file" class="file-input" name="url">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Multiple Hobbies</label>
                        <div class="col-lg-10">
                            <select multiple="multiple" class="form-control"  name="hobbies[]">
                                <option  value="Cricket">Cricket</option>
                                <option  value="Travelling">Travelling</option>
                                <option  value="Reading">Reading</option>
                                <option  value="Singing">Singing</option>
                                <option  value="Trekking">Trekking</option>
                            </select>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-2">Gender</label>
                        <div class="col-lg-10">
                            <input type="radio" name="gender" class="styled"  value="male" checked="checked">Male
                            <input type="radio" name="gender" class="styled" value="female">Female
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-2">Subjects</label>
                        <div class="col-lg-10">
                            <input type="checkbox" class="styled" checked="checked" name="sub1" value="English"> English
                            <br><input type="checkbox" class="styled" checked="checked" name="sub2" value="Maths"> Maths
                            <br><input type="checkbox" class="styled" checked="checked" name="sub3" value="Science"> Science
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-2">Username</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="button" name="usernamePrepend" value="wmt">wmt-</button>
                                    <input type="text" name="usernamePrepend" value="wmt-" hidden>
                                </span>
                                <input type="text" class="form-control" name="username"  placeholder="Username starts with wmt-">
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-lg-2">Email</label>
                        <div class="col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Email" name="email">
                                <input type="text" value="@webmob.tech" name="emailAppend" hidden>
                                <span class="input-group-btn">
								    <button class="btn btn-default" value="@webmob.tech" name="emailAppend" type="button">@webmob.tech</button>
                                </span>
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-lg-2">Address</label>
                        <div class="col-lg-10">
                            <textarea name="address" class="form-control" placeholder="Address" cols="30" rows="8"></textarea>

                        </div>
                    </div>

                    <div class="text-center">
                    <button type="submit" class="btn btn-float btn-danger">Submit Form</button>
                    </div>



                </fieldset>
            </form>
        </div>


    </div>


@endsection
