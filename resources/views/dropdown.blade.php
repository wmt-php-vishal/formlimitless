<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />


    <title>Dropdown</title>
</head>
<body>

<div class="container">
    <h2>Dynamic Dropdown</h2>
    <form id="form-data" action="/getData">

    <div class="form-group">
        <label for="state">States</label>
        <select name="state" id="state">
            <option value="">All states</option>

            @foreach($states as $key => $value)

                <option value="{{$key}}">{{$value}}</option>
                @endforeach

        </select>
    </div>
</div>

<br>
    <div class="form-group">
        <label for="city" hidden class="label">Cities</label>
        <select name="city" id="city" hidden>
            <option value="">All Cities</option>

        </select>
    </div>
</div>


    <input type="submit" name="button_action" id="button_action" value="insert" class="btn btn-info" />
</form>

{{--<a href="/getData" name="" value class="">Submit</a>--}}


<script src="{{asset('assets/js/jquery.js')}}"></script>


<script>
    // $('select[name="city').hide();

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
    $(document).ready(function () {

        $('select[name="state"]').on('change', function () {
            var state_id = $(this).val();
            var url = '{{ route('getCities',['id' => ':id']) }}';

            url = url.replace(':id', $(this).val());
            if (state_id) {

                $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    success: function (data) {
                        $('select[name="city').show();
                        $('.label').show();

                        $('select[name="city').empty();
                        $.each(data, function (key, values) {
                            $('select[name="city"]').append('<option value="' + key + '">' + values + '</option>');

                        });
                        console.log(data);

                    }
                })
            }

        });

        $('#form-data').submit(function (e) {

            var state = $('#state').val();
            var city = $('#city').val();

            $.ajax({
                url: '/getData',
                type: 'post',
                data: {_token: CSRF_TOKEN, state: state, city: city}

            });

        });

    });

</script>

</body>
</html>
